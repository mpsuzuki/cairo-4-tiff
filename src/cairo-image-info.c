/* -*- Mode: c; tab-width: 8; c-basic-offset: 4; indent-tabs-mode: t; -*- */
/* cairo - a vector graphics library with display and print output
 *
 * Copyright © 2008 Adrian Johnson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation
 * (the "LGPL") or, at your option, under the terms of the Mozilla
 * Public License Version 1.1 (the "MPL"). If you do not alter this
 * notice, a recipient may use your version of this file under either
 * the MPL or the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-2.1; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 * You should have received a copy of the MPL along with this library
 * in the file COPYING-MPL-1.1
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL or the MPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the cairo graphics library.
 *
 * The Initial Developer of the Original Code is Adrian Johnson.
 *
 * Contributor(s):
 *	Adrian Johnson <ajohnson@redneon.com>
 */

#include "cairoint.h"

#include "cairo-error-private.h"
#include "cairo-image-info-private.h"

/* JPEG (image/jpeg)
 *
 * http://www.w3.org/Graphics/JPEG/itu-t81.pdf
 */

/* Markers with no parameters. All other markers are followed by a two
 * byte length of the parameters. */
#define TEM       0x01
#define RST_begin 0xd0
#define RST_end   0xd7
#define SOI       0xd8
#define EOI       0xd9

/* Start of frame markers. */
#define SOF0  0xc0
#define SOF1  0xc1
#define SOF2  0xc2
#define SOF3  0xc3
#define SOF5  0xc5
#define SOF6  0xc6
#define SOF7  0xc7
#define SOF9  0xc9
#define SOF10 0xca
#define SOF11 0xcb
#define SOF13 0xcd
#define SOF14 0xce
#define SOF15 0xcf

static const unsigned char *
_jpeg_skip_segment (const unsigned char *p)
{
    int len;

    p++;
    len = (p[0] << 8) | p[1];

    return p + len;
}

static void
_jpeg_extract_info (cairo_image_info_t *info, const unsigned char *p)
{
    info->width = (p[6] << 8) + p[7];
    info->height = (p[4] << 8) + p[5];
    info->num_components = p[8];
    info->bits_per_component = p[3];
}

cairo_int_status_t
_cairo_image_info_get_jpeg_info (cairo_image_info_t	*info,
				 const unsigned char	*data,
				 long			 length)
{
    const unsigned char *p = data;

    while (p + 1 < data + length) {
	if (*p != 0xff)
	    return CAIRO_INT_STATUS_UNSUPPORTED;
	p++;

	switch (*p) {
	    /* skip fill bytes */
	case 0xff:
	    p++;
	    break;

	case TEM:
	case SOI:
	case EOI:
	    p++;
	    break;

	case SOF0:
	case SOF1:
	case SOF2:
	case SOF3:
	case SOF5:
	case SOF6:
	case SOF7:
	case SOF9:
	case SOF10:
	case SOF11:
	case SOF13:
	case SOF14:
	case SOF15:
	    /* Start of frame found. Extract the image parameters. */
	    if (p + 8 > data + length)
		return CAIRO_INT_STATUS_UNSUPPORTED;

	    _jpeg_extract_info (info, p);
	    return CAIRO_STATUS_SUCCESS;

	default:
	    if (*p >= RST_begin && *p <= RST_end) {
		p++;
		break;
	    }

	    if (p + 2 > data + length)
		return CAIRO_INT_STATUS_UNSUPPORTED;

	    p = _jpeg_skip_segment (p);
	    break;
	}
    }

    return CAIRO_STATUS_SUCCESS;
}

/* JPEG 2000 (image/jp2)
 *
 * http://www.jpeg.org/public/15444-1annexi.pdf
 */

#define JPX_FILETYPE 0x66747970
#define JPX_JP2_HEADER 0x6A703268
#define JPX_IMAGE_HEADER 0x69686472

static const unsigned char _jpx_signature[] = {
    0x00, 0x00, 0x00, 0x0c, 0x6a, 0x50, 0x20, 0x20, 0x0d, 0x0a, 0x87, 0x0a
};

static const unsigned char *
_jpx_next_box (const unsigned char *p)
{
    return p + get_unaligned_be32 (p);
}

static const unsigned char *
_jpx_get_box_contents (const unsigned char *p)
{
    return p + 8;
}

static cairo_bool_t
_jpx_match_box (const unsigned char *p, const unsigned char *end, uint32_t type)
{
    uint32_t length;

    if (p + 8 < end) {
	length = get_unaligned_be32 (p);
	if (get_unaligned_be32 (p + 4) == type &&  p + length < end)
	    return TRUE;
    }

    return FALSE;
}

static const unsigned char *
_jpx_find_box (const unsigned char *p, const unsigned char *end, uint32_t type)
{
    while (p < end) {
	if (_jpx_match_box (p, end, type))
	    return p;
	p = _jpx_next_box (p);
    }

    return NULL;
}

static void
_jpx_extract_info (const unsigned char *p, cairo_image_info_t *info)
{
    info->height = get_unaligned_be32 (p);
    info->width = get_unaligned_be32 (p + 4);
    info->num_components = (p[8] << 8) + p[9];
    info->bits_per_component = p[10];
}

cairo_int_status_t
_cairo_image_info_get_jpx_info (cairo_image_info_t	*info,
				const unsigned char	*data,
				unsigned long		 length)
{
    const unsigned char *p = data;
    const unsigned char *end = data + length;

    /* First 12 bytes must be the JPEG 2000 signature box. */
    if (length < ARRAY_LENGTH(_jpx_signature) ||
	memcmp(p, _jpx_signature, ARRAY_LENGTH(_jpx_signature)) != 0)
	return CAIRO_INT_STATUS_UNSUPPORTED;

    p += ARRAY_LENGTH(_jpx_signature);

    /* Next box must be a File Type Box */
    if (! _jpx_match_box (p, end, JPX_FILETYPE))
	return CAIRO_INT_STATUS_UNSUPPORTED;

    p = _jpx_next_box (p);

    /* Locate the JP2 header box. */
    p = _jpx_find_box (p, end, JPX_JP2_HEADER);
    if (!p)
	return CAIRO_INT_STATUS_UNSUPPORTED;

    /* Step into the JP2 header box. First box must be the Image
     * Header */
    p = _jpx_get_box_contents (p);
    if (! _jpx_match_box (p, end, JPX_IMAGE_HEADER))
	return CAIRO_INT_STATUS_UNSUPPORTED;

    /* Get the image info */
    p = _jpx_get_box_contents (p);
    _jpx_extract_info (p, info);

    return CAIRO_STATUS_SUCCESS;
}

/* PNG (image/png)
 *
 * http://www.w3.org/TR/2003/REC-PNG-20031110/
 */

#define PNG_IHDR 0x49484452

static const unsigned char _png_magic[8] = { 137, 80, 78, 71, 13, 10, 26, 10 };

cairo_int_status_t
_cairo_image_info_get_png_info (cairo_image_info_t     *info,
                               const unsigned char     *data,
                               unsigned long            length)
{
    const unsigned char *p = data;
    const unsigned char *end = data + length;

    if (length < 8 || memcmp (data, _png_magic, 8) != 0)
       return CAIRO_INT_STATUS_UNSUPPORTED;

    p += 8;

    /* The first chunk must be IDHR. IDHR has 13 bytes of data plus
     * the 12 bytes of overhead for the chunk. */
    if (p + 13 + 12 > end)
       return CAIRO_INT_STATUS_UNSUPPORTED;

    p += 4;
    if (get_unaligned_be32 (p) != PNG_IHDR)
       return CAIRO_INT_STATUS_UNSUPPORTED;

    p += 4;
    info->width = get_unaligned_be32 (p);
    p += 4;
    info->height = get_unaligned_be32 (p);

    return CAIRO_STATUS_SUCCESS;
}

static const unsigned char *
_jbig2_find_data_end (const unsigned char *p,
		      const unsigned char *end,
		      int                  type)
{
    unsigned char end_seq[2];
    int mmr;

    /* Segments of type "Immediate generic region" may have an
     * unspecified data length.  The JBIG2 specification specifies the
     * method to find the end of the data for these segments. */
    if (type == 36 || type == 38 || type == 39) {
	if (p + 18 < end) {
	    mmr = p[17] & 0x01;
	    if (mmr) {
		/* MMR encoding ends with 0x00, 0x00 */
		end_seq[0] = 0x00;
		end_seq[1] = 0x00;
	    } else {
		/* Template encoding ends with 0xff, 0xac */
		end_seq[0] = 0xff;
		end_seq[1] = 0xac;
	    }
	    p += 18;
	    while (p < end) {
		if (p[0] == end_seq[0] && p[1] == end_seq[1]) {
		    /* Skip the 2 terminating bytes and the 4 byte row count that follows. */
		    p += 6;
		    if (p < end)
			return p;
		}
		p++;
	    }
	}
    }

    return NULL;
}

static const unsigned char *
_jbig2_get_next_segment (const unsigned char  *p,
			 const unsigned char  *end,
			 int                  *type,
			 const unsigned char **data,
			 unsigned long        *data_len)
{
    unsigned long seg_num;
    cairo_bool_t big_page_size;
    int num_segs;
    int ref_seg_bytes;
    int referred_size;

    if (p + 6 >= end)
	return NULL;

    seg_num = get_unaligned_be32 (p);
    *type = p[4] & 0x3f;
    big_page_size = (p[4] & 0x40) != 0;
    p += 5;

    num_segs = p[0] >> 5;
    if (num_segs == 7) {
	num_segs = get_unaligned_be32 (p) & 0x1fffffff;
	ref_seg_bytes = 4 + ((num_segs + 1)/8);
    } else {
	ref_seg_bytes = 1;
    }
    p += ref_seg_bytes;

    if (seg_num <= 256)
	referred_size = 1;
    else if (seg_num <= 65536)
	referred_size = 2;
    else
	referred_size = 4;

    p += num_segs * referred_size;
    p += big_page_size ? 4 : 1;
    if (p + 4 >= end)
	return NULL;

    *data_len = get_unaligned_be32 (p);
    p += 4;
    *data = p;

    if (*data_len == 0xffffffff) {
	/* if data length is -1 we have to scan through the data to find the end */
	p = _jbig2_find_data_end (*data, end, *type);
	if (!p || p >= end)
	    return NULL;

	*data_len = p - *data;
    } else {
	p += *data_len;
    }

    if (p < end)
	return p;
    else
	return NULL;
}

static void
_jbig2_extract_info (cairo_image_info_t *info, const unsigned char *p)
{
    info->width = get_unaligned_be32 (p);
    info->height = get_unaligned_be32 (p + 4);
    info->num_components = 1;
    info->bits_per_component = 1;
}

cairo_int_status_t
_cairo_image_info_get_jbig2_info (cairo_image_info_t	*info,
				  const unsigned char	*data,
				  unsigned long		 length)
{
    const unsigned char *p = data;
    const unsigned char *end = data + length;
    int seg_type;
    const unsigned char *seg_data;
    unsigned long seg_data_len;

    while (p && p < end) {
	p = _jbig2_get_next_segment (p, end, &seg_type, &seg_data, &seg_data_len);
	if (p && seg_type == 48 && seg_data_len > 8) {
	    /* page information segment */
	    _jbig2_extract_info (info, seg_data);
	    return CAIRO_STATUS_SUCCESS;
	}
    }

    return CAIRO_INT_STATUS_UNSUPPORTED;
}


static unsigned long
_tiff_get_uint8_from_buffer(const unsigned char  *data)
{
    return data[0];
}

static unsigned long
_tiff_get_uint16_from_le_buffer(const unsigned char  *data)
{
    return (data[1] << 8) + data[0];
}

static unsigned long
_tiff_get_uint16_from_be_buffer(const unsigned char  *data)
{
    return (data[0] << 8) + data[1];
}

static unsigned long
_tiff_get_uint32_from_le_buffer(const unsigned char  *data)
{
    return (data[3] << 24) + (data[2] << 16) + (data[1] << 8) + data[0];
}

static unsigned long
_tiff_get_uint32_from_be_buffer(const unsigned char  *data)
{
    return (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
}

typedef unsigned long(*_tiff_get_uint_func_t)(const unsigned char*);

typedef enum {
    _TIFF_VALUE_VARIABLE = 0,
    _TIFF_VALUE_8BIT,
    _TIFF_VALUE_16BIT,
    _TIFF_VALUE_24BIT, /* until TIFF 6.0, not used */
    _TIFF_VALUE_32BIT,
    _TIFF_VALUE_64BIT
} _tiff_value_length_t;
 
static _tiff_get_uint_func_t
_tiff_get_uint_funcs[6][2] =  {
    {NULL, NULL}, /* variable length */
    {&_tiff_get_uint8_from_buffer,     &_tiff_get_uint8_from_buffer },
    {&_tiff_get_uint16_from_le_buffer, &_tiff_get_uint16_from_be_buffer},
    {NULL, NULL}, /* 24-bit */
    {&_tiff_get_uint32_from_le_buffer, &_tiff_get_uint32_from_be_buffer},
    {NULL, NULL}, /* 64-bit, non-immediate values only */
};

typedef enum {
    _TIFF_LITTLE_ENDIAN = 0,
    _TIFF_BIG_ENDIAN,
    _TIFF_INVALID_ENDIAN
} _tiff_endian_t;

#define _TIFF_HEADER_MAGIC 42

typedef struct {
    cairo_image_info_t  *cairo_image_info;
    _tiff_endian_t       endian;
    unsigned short       compressionType;
    unsigned long        stripOffset;
    unsigned long        stripByteCount;
    unsigned long        rowsPerStrip;
    unsigned short       resolutionUnit;
    unsigned long        offsetToXResolution;
    unsigned long        offsetToYResolution;
    unsigned long        xResolutionFrac;
    unsigned long        xResolutionDenom;
    unsigned long        yResolutionFrac;
    unsigned long        yResolutionDenom;
} _tiff_info_t;

char fieldType2octetSize[] = { 0, 0xFF, 1, 2, 4, 8, 1, 0xFF, 2, 4, 8, 4, 8 };

static cairo_int_status_t
_cairo_image_info_tiff_parse_ifd_item(_tiff_info_t         *tiff_info,
                                      const unsigned char  *p)
{
    unsigned short  fieldTag  = (_tiff_get_uint_funcs[_TIFF_VALUE_16BIT][tiff_info->endian])(p);
    unsigned short  fieldType = (_tiff_get_uint_funcs[_TIFF_VALUE_16BIT][tiff_info->endian])(p + 2);
    unsigned long   numValues = (_tiff_get_uint_funcs[_TIFF_VALUE_32BIT][tiff_info->endian])(p + 4);
    unsigned long   firstValue = 0;
    int             sizeOfValue;
    int             valueIsOffset = 0;


    /* Maybe newer TIFF has unknown fieldType, but we should not abort */
    if (sizeof(fieldType2octetSize) < fieldType)
        return CAIRO_STATUS_SUCCESS;

    sizeOfValue = fieldType2octetSize[fieldType];
#if 0
    /* Currently we care only TIFF file including single CCITT stream,
       so all required properties should be stored as immediate values.
       Non-immediate value cases should be ignored, but we should not abort */
    if (sizeOfValue * numValues > 4)
        return CAIRO_STATUS_SUCCESS;

    /* We care 16-bit or 32-bit properties, no float, no string*/
    if (sizeOfValue < 0 || 4 < sizeOfValue)
        return CAIRO_STATUS_SUCCESS;
#else
    if (sizeOfValue < 0)
        return CAIRO_STATUS_SUCCESS;

    if (4 < sizeOfValue)
        valueIsOffset = 1;
#endif

    /* We care very simple TIFF file; single page, single strip, ... only taking first value */
    firstValue = (_tiff_get_uint_funcs[ (valueIsOffset ? 4 : sizeOfValue) ][tiff_info->endian])(p + 8);

    switch (fieldTag) {
        case 0x0100: /* ImageWidth */
            if (numValues > 1)
                return CAIRO_INT_STATUS_UNSUPPORTED;
            tiff_info->cairo_image_info->width = firstValue;
            break;
        case 0x0101: /* ImageLength */
            if (numValues > 1)
                return CAIRO_INT_STATUS_UNSUPPORTED;
            tiff_info->cairo_image_info->height = firstValue;
            break;
        case 0x0102: /* BitsPerSample */
            if (numValues > 1)
                return CAIRO_INT_STATUS_UNSUPPORTED;
            tiff_info->cairo_image_info->bits_per_component = firstValue;
            tiff_info->cairo_image_info->num_components = firstValue;
            break;
        case 0x0103: /* Compression */
            if (numValues > 1)
                return CAIRO_INT_STATUS_UNSUPPORTED;
            tiff_info->compressionType = firstValue;
            break;
        case 0x0111: /* StripOffsets */
            if (numValues > 1)
                return CAIRO_INT_STATUS_UNSUPPORTED;
            tiff_info->stripOffset = firstValue;
            break;
#if 0
	/* Bi-level TIFF does not require this IFD */ 
        case 0x0115: /* SamplesPerPixel */
            if (numValues > 1)
                return CAIRO_INT_STATUS_UNSUPPORTED;
            tiff_info->cairo_image_info->num_components = firstValue;
            break;
#endif
        case 0x0116: /* rowsPerStrip */
            if (numValues > 1)
                return CAIRO_INT_STATUS_UNSUPPORTED;
            tiff_info->rowsPerStrip = firstValue;
            break;
        case 0x0117: /* stripByteCounts */
            if (numValues > 1)
                return CAIRO_INT_STATUS_UNSUPPORTED;
            tiff_info->stripByteCount = firstValue;
            break;
	case 0x011A:  /* xResolution */
            if (numValues > 1 && !valueIsOffset)
                return CAIRO_INT_STATUS_UNSUPPORTED;
	    tiff_info->offsetToXResolution = firstValue;
            break;
	case 0x011B:  /* yResolution */
            if (numValues > 1 && !valueIsOffset)
                return CAIRO_INT_STATUS_UNSUPPORTED;
	    tiff_info->offsetToYResolution = firstValue;
            break;
        case 0x0124: /* T4Options (options for G3 Fax data) */
            if (numValues > 1)
                return CAIRO_INT_STATUS_UNSUPPORTED;
            if ((firstValue & 1) > 0) /* 2-D encoding, TIFF 6.0 spec p.51 */
                return CAIRO_INT_STATUS_UNSUPPORTED;
            if ((firstValue & 2) > 0) /* Uncompressed mode, see TIFF 6.0 spec p.52 */
                return CAIRO_INT_STATUS_UNSUPPORTED;
            break;
        case 0x0125: /* T6Options (options for G4 Fax data) */
            if (numValues > 1)
                return CAIRO_INT_STATUS_UNSUPPORTED;
            if ((firstValue & 2) > 0) /* Uncompressed mode, see TIFF 6.0 spec p.52 */
                return CAIRO_INT_STATUS_UNSUPPORTED;
            break;
        case 0x0128: /* Resolution unit */
            if (numValues > 1)
                return CAIRO_INT_STATUS_UNSUPPORTED;
            tiff_info->resolutionUnit = firstValue;
            break;
    }

    return CAIRO_STATUS_SUCCESS;
}


static unsigned long
_cairo_image_info_tiff_parse_ifd_list (_tiff_info_t         *tiff_info,
                                       const unsigned char  *ifd_head,
                                       const unsigned char  *ifd_end)
{
    const unsigned char  *p = ifd_head;
    unsigned short        numIFDItems;
    int                   i;

    if (ifd_end - ifd_head < 2)
      return 0;

    numIFDItems = (_tiff_get_uint_funcs[_TIFF_VALUE_16BIT][tiff_info->endian])(p);
    p += 2;
    
    for (i = 0; i < numIFDItems; i ++)
    {
      if (ifd_end < p + 12)
        return 0;
      if (_cairo_image_info_tiff_parse_ifd_item(tiff_info, p))
        return 0;
      p += 12;
    }

    if (ifd_end < p + 4)
      return 0;

    return (_tiff_get_uint_funcs[_TIFF_VALUE_32BIT][tiff_info->endian])(p);
}

cairo_int_status_t
_cairo_image_info_get_ccitt_info_from_tiff (cairo_image_info_t	 *info,
                                            const unsigned char  *p,
                                            unsigned long         length,
                                            unsigned short       *ccitt_compression_type,
                                            unsigned long        *ccitt_stream_offset,
                                            unsigned long        *ccitt_stream_length,
                                            unsigned long        *ccitt_stream_columns,
                                            unsigned long        *ccitt_stream_rows)
{
    _tiff_info_t    tiff_info = {info, _TIFF_INVALID_ENDIAN, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    unsigned long   offsetToIFD = 0;

    info->width = 0;
    info->height = 0;
    info->num_components = 0;
    info->bits_per_component = 0;

    if (length < 8) /* TIFF header requires 8 octets */
        return CAIRO_INT_STATUS_UNSUPPORTED;

    if (p[0] == 'I' && p[1] == 'I')
        tiff_info.endian = _TIFF_LITTLE_ENDIAN;
    else if (p[0] == 'M' && p[1] == 'M')
        tiff_info.endian = _TIFF_BIG_ENDIAN;
    else
        return CAIRO_INT_STATUS_UNSUPPORTED;

    if (_TIFF_HEADER_MAGIC != (_tiff_get_uint_funcs[_TIFF_VALUE_16BIT][tiff_info.endian])(p + 2))
        return CAIRO_INT_STATUS_UNSUPPORTED;

    offsetToIFD = (_tiff_get_uint_funcs[_TIFF_VALUE_32BIT][tiff_info.endian])(p + 4);
    while ( 0 < offsetToIFD && offsetToIFD < length ) {
        offsetToIFD = _cairo_image_info_tiff_parse_ifd_list(&tiff_info,
                                                            p + offsetToIFD,
                                                            p + length);
    }

    tiff_info.xResolutionFrac  = (_tiff_get_uint_funcs[_TIFF_VALUE_32BIT][tiff_info.endian])(p + tiff_info.offsetToXResolution);
    tiff_info.xResolutionDenom = (_tiff_get_uint_funcs[_TIFF_VALUE_32BIT][tiff_info.endian])(p + tiff_info.offsetToXResolution + 4);
    tiff_info.yResolutionFrac  = (_tiff_get_uint_funcs[_TIFF_VALUE_32BIT][tiff_info.endian])(p + tiff_info.offsetToYResolution);
    tiff_info.yResolutionDenom = (_tiff_get_uint_funcs[_TIFF_VALUE_32BIT][tiff_info.endian])(p + tiff_info.offsetToYResolution + 4);

    /* Fail if some fields are not filled after parsing all IFD lists */
    if (!info->width || !info->height || !info->num_components || !info->bits_per_component ||
        !tiff_info.stripOffset || !tiff_info.stripByteCount)
        return CAIRO_INT_STATUS_UNSUPPORTED;

    *ccitt_compression_type = tiff_info.compressionType;
    *ccitt_stream_offset    = tiff_info.stripOffset;
    *ccitt_stream_length    = tiff_info.stripByteCount;
    *ccitt_stream_columns   = info->width;
    *ccitt_stream_rows      = (info->width >= 0 && tiff_info.rowsPerStrip == (unsigned long)info->width) ? 0 : tiff_info.rowsPerStrip;
    return CAIRO_STATUS_SUCCESS;
}
